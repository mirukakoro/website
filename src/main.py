# [START gae_python37_app]
from flask import *
import csv
import time
import json
from datetime import time
import lib


config_file_path = 'config.json'
with open(config_file_path, 'r') as file:
  config = json.load(file)

app = Flask(__name__)

stop_db = config['stop_db']

@app.route('/')
def index():
  return render_template('index.html')

@app.route('/about')
def about():
  return render_template('about.html')

@app.route('/hamster/pick')
def agency_choose():
  url_format = config['url_format']['agency_choose']
  copyright_format = config['copyright_format']
  data = lib._get(url_format)
  agencies = data['agency']
  routes_url = '/hamster/{}/pick'
  notices = [{'text':copyright_format.format(data['copyright'])}]
  return render_template('pick_a.html', agencies = agencies, routes_url = routes_url,
                         notices = notices)

@app.route('/hamster/<a>/pick')
def route_choose(a):
  url_format = config['url_format']['route_choose']
  copyright_format = config['copyright_format']
  data = lib._get(url_format.format(a))
  print(data)
  routes = data['route']
  routes_url = '/hamster/{}'.format(a) + '/{}/pick'
  notices = [{'text':copyright_format.format(data['copyright'])}]
  return render_template('pick_r.html', routes = routes, routes_url = routes_url, a = a,
                         notices = notices)

@app.route('/hamster/<a>/<r>/pick')
def stop_choose(a, r):
  url_format = config['url_format']['stop_choose']
  copyright_format = config['copyright_format']
  data = lib._get(url_format.format(a, r))
  stops_pool = data['route']['stop']
  routes_pool = data['route']['direction']
  result = []
  stops = {}
  for stop in stops_pool:
    stops[stop['tag']] = stop
    stop_db[stop['tag']] = stop
  with open(config_file_path, 'w') as file:
    json.dump(config, file)
  del stops_pool
  if type(routes_pool) == type(list()):
    for route in routes_pool:
      pool = {}
      pool['stops'] = []
      pool['ui'] = route['useForUI']
      pool['tag'] = route['tag']
      pool['title'] = route['title']
      pool['name'] = route['name']
      if 'branch' not in route.keys():
        pool['branch'] = None
      else:
        pool['branch'] = route['branch']
      for stop in route['stop']:
        pool['stops'].append(stops[stop['tag']])
      result.append(pool)
  else:
    pool = {}
    pool['stops'] = []
    pool['ui'] = routes_pool['useForUI']
    pool['tag'] = routes_pool['tag']
    pool['title'] = routes_pool['title']
    pool['name'] = routes_pool['name']
    if 'branch' not in routes_pool.keys():
      pool['branch'] = None
    else:
      pool['branch'] = routes_pool['branch']
    for stop in routes_pool['stop']:
      pool['stops'].append(stops[stop['tag']])
    result.append(pool)
  stop_url = '/hamster/{}/{}'.format(a, r) + '/{}'
  notices = [{'text':copyright_format.format(data['copyright'])}]
  return render_template('pick_s.html', result = result, stop_url = stop_url, r = r,
                         notices = notices)

@app.route('/hamster/<a>/<r>/<s>')
def stop_show(a, r, s):
  url_format_0 = config['url_format']['stop_show_0']
  url_format_1 = config['url_format']['stop_show_1']
  copyright_format = config['copyright_format']
  if s in stop_db.keys() and 'stopId' in stop_db[s]:
    stop_id = stop_db[s]['stopId']
    data = lib._get(url_format_0.format(a, stop_id))
  else:
    stop_id = None
    data = lib._get(url_format_1.format(a, r, s))
  print(data)
  notices = [{'text': copyright_format.format(data['copyright'])}]
  result = []
  if type(data['predictions']) == type(dict()):
    if 'dirTitleBecauseNoPredictions' in data['predictions'].keys():
      preds = None
      pool = {'agency': data['predictions']['agencyTitle'], 'r': data['predictions']['routeTag'],
              'r_t': data['predictions']['routeTitle'], 's_t': data['predictions']['stopTitle'],
              's': data['predictions']['stopTag'], 's_i': stop_id, 'preds': preds,
              'preds_prediction_dict': None}
    else:
      preds = data['predictions']['direction']
      pool = {'agency': data['predictions']['agencyTitle'], 'r': data['predictions']['routeTag'],
              'r_t': data['predictions']['routeTitle'], 's_t': data['predictions']['stopTitle'],
              's': data['predictions']['stopTag'], 's_i': stop_id, 'preds': preds,
              'preds_prediction_dict': type(preds['prediction']) == type(dict())}
    result.append(pool)
  else:
    for prediction in data['predictions']:
      if 'dirTitleBecauseNoPredictions' in prediction.keys():
        preds = None
        pool = {'agency': prediction['agencyTitle'], 'r': prediction['routeTag'], 'r_t': prediction['routeTitle'],
                's_t': prediction['stopTitle'], 's': prediction['stopTag'], 's_i': stop_id, 'preds': preds}
      else:
        preds = prediction['direction']
        pool = {'agency': prediction['agencyTitle'], 'r': prediction['routeTag'], 'r_t':prediction['routeTitle'],
                's_t': prediction['stopTitle'], 's': prediction['stopTag'], 's_i': stop_id, 'preds': preds,
              'preds_prediction_dict': type(preds['prediction']) == type(dict())}
      result.append(pool)
  return render_template('show_s.html', stop = stop_db[s], result = result,
                         notices=notices)


@app.errorhandler(404)
def error404(error):
  return render_template('error.html', code='404', short='Route not found.',
                         long='There were no responses for URL "{}".'.format(request.url))

@app.errorhandler(500)
def error500(error):
  return render_template('error.html', code='500', short='Internal Error',
                         long='Error encountered while processing request.')

@app.errorhandler(403)
def error403(error):
  return render_template('error.html', code='403', short='Forbidden',
                         long='Unable to access forbidden route.')

@app.errorhandler(410)
def error410(error):
  return render_template('error.html', code='410', short='Gone',
                         long='Unable to access gone route.')


if __name__ == '__main__':
  app.run(host='0.0.0.0', port=8080, debug=False, use_evalex=False)
# [END gae_python37_app]
